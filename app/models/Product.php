<?php
	class Product {
		private $db;

		public function __construct() {
			$this->db = new Database;
		}
		//Get the products from the database
		public function getBooks() {
			$this->db->query('SELECT * FROM books');

			$results = $this->db->resultSet();
			return $results;
		}
		public function getDvd() {
			$this->db->query('SELECT * FROM dvd');

			$results = $this->db->resultSet();
			return $results;
		}
		public function getFurniture() {
			$this->db->query('SELECT * FROM furniture');

			$results = $this->db->resultSet();
			return $results;
		}

		//Method for product add
		public function addProduct($data) {
			$this->db->query('INSERT INTO '.$data['tableName'].'(SKU, name, price, '.$data['attrName'].') VALUES (:SKU, :name, :price, :attrValue)');
			// Bind values
			$this->db->bind(':SKU', $data['SKU']);
			$this->db->bind(':name', $data['name']);
			$this->db->bind(':price', $data['price']);
			$this->db->bind(':attrValue', $data['attrValue']);

			// Execute
			if ($this->db->execute()) {
				return true;
			} else {
				return false;
			}
		}
		
		//Method for product delete
		public function deleteProduct($checked, $tableName) {
			foreach ($checked as $id) {
			$this->db->query('DELETE FROM '.$tableName.' WHERE id = :id');
			// Bind values
			$this->db->bind(':id', $id);
			// Execute
			$this->db->execute();
			}
		}
		//Method for checking if the SKU already exists
		public function SKUcheck($SKU) {
		$tables = array("dvd", "books", "furniture");
			for ($i=0; $i < count($tables); $i++) { 
				$this->db->query('SELECT * FROM '.$tables[$i].' WHERE SKU = :SKU');
				$this->db->bind(':SKU', $SKU);
				$this->db->execute();
				$result = $this->db->rowCount();
					if ($result) {
						return true;
					}
				}
				return false;
		}
	}