<?php
	/*
   * App Core Class
   * Creates URL & loads core controller
   * URL FORMAT - /controller/method/params
   */
	class Core
	{
		protected $currentControllers = 'Products';
		protected $currentMethod = 'index';
		protected $params = [];

		public function __construct()
		{
			$url = $this->getUrl();

			// Look in controllers for first value
			if (isset($_GET['url'])) {
				if (file_exists('../app/controllers/' . ucwords($url[0]).'.php')) {
					//If exists, set as controller
					$this->currentControllers = ucwords($url[0]);
					//Unset 0 Index
					unset($url[0]);
				}
			}
			
			// Require the controller
			require_once '../app/controllers/'.$this->currentControllers. '.php';
			//Instantiate controller class
			$this->currentControllers = new $this->currentControllers;

			//Check for second part of url
			if (isset($url[1])) {
				//Check to see if method exists in controller
				if (method_exists($this->currentControllers, $url[1])) {
					$this->currentMethod = $url[1];
					//Unset 1 index
					unset($url[1]);
				}
			}

			//Get params
			$this->params = $url ? array_values($url) : [];

			// Call a callback with array of params
			call_user_func_array([$this->currentControllers, $this->currentMethod], $this->params);

		}

		public function getUrl()
		{
			if (isset($_GET['url'])) {
				//Clean and filter incoming url
				$url = rtrim($_GET['url'], '/');
				$url = filter_var($url, FILTER_SANITIZE_URL);
				$url = explode('/', $url);
				return $url;
			}
		}
	}