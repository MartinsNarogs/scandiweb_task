<?php
	//DB Params
	define('DB_HOST', 'localhost');
	define('DB_USER', 'root');
	define('DB_PASS', '');
	define('DB_NAME', 'products');

	//App Root
	define('APPROOT', dirname(dirname(__FILE__)));

	//URL Root
	define('URLROOT', 'http://localhost/Scandiwebtest');

	//Site Name
	define('SITENAME', 'Scandiwebtest');