<?php
	//Load the config file
	require_once 'config/config.php';
	//Load the helper file
	require_once 'helpers/url_helper.php';
	//Load the interface
	require_once 'helpers/interface.php';

	//Autoload the main libraries
	spl_autoload_register(function($className) {
		require_once 'libraries/'.$className.'.php';
	});