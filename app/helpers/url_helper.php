<?php 
	//Simple page redirect
	function redirect($page) {
		header('location: '. URLROOT . '/' . $page);
	}
	//Function for checking up a input data
	function check_input($data) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}
