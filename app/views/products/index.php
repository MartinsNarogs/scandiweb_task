<?php require APPROOT . '/views/inc/header.php'; ?>
 <form action="<?php echo URLROOT; ?>/products/delete" method="post" >
 	<div class="d-flex  p-3 px-md-4 mb-3 justify-content-between">
        <h3 class="my-0 mr-md-auto font-weight-normal">Product list</h3>
        <div class="my-2 my-md-0 mr-md-3">
        	<select id="listOptions" name="listOptions">
	          	<!-- The  option stands for deleting all the selected products with a "checkbox" -->
	          	<option value="delete">Delete Mass Action</option>
        	</select>
        	<button type="submit" name="apply" class="btn btn-success">Apply</button>
        </div>
    </div>
    <hr>
   	<?php if ($data['dvd']) : ?>
	<h3 class="mb-4 mt-5">DVD-Discs</h3>
	<div class="d-flex flex-wrap">
		<?php foreach ($data['dvd'] as $dvd) : 	?>
		 <div class="card mb-4 shadow-sm">
	      <div class="card-header d-flex justify-content-between">
	        <h4 class="my-0 font-weight-normal"><?= $dvd['SKU']; ?></h4>
	        <input type="checkbox" class="checkbox" name="dvdbox[]" value="<?= $dvd['id']; ?>">
	      </div>
	      <div class="card-body">
	       <h2 class="card-title text-center"><?= $dvd['name']; ?></h2>
	        <p class="mt-3 mb-3 text-center"><strong>Price:</strong> <?= $dvd['price']; ?> $</p>
	        <p class="mt-2 mb-3 text-center"><strong>Size:</strong> <?= $dvd['size']; ?> MB</p>
	      </div>
	    </div>
	<?php endforeach; ?>
	</div>
	<?php endif;
		  if ($data['books']) :	?>
	<h3 class="mb-4">Books</h3>
	<div class="d-flex flex-wrap">
		<?php	foreach ($data['books'] as $book) : 	?>
		 <div class="card mb-4 shadow-sm">
	      <div class="card-header d-flex justify-content-between">
	        <h4 class="my-0 font-weight-normal"><?= $book['SKU']; ?></h4>
	        <input type="checkbox" class="checkbox" name="bookbox[]" value="<?= $book['id']; ?>">
	      </div>
	      <div class="card-body">
	       <h2 class="card-title text-center"><?= $book['name']; ?></h2>
	        <p class="mt-3 mb-3 text-center"><strong>Price:</strong> <?= $book['price']; ?> $</p>
	        <p class="mt-2 mb-3 text-center"><strong>Weight:</strong> <?= $book['weight']; ?> Kg</p>
	      </div>
	    </div>
	<?php endforeach; ?>
	</div>
	<?php endif;
		  if ($data['furniture']) : ?>
	<h3 class="mb-4">Furniture</h3>
	<div class="d-flex flex-wrap">
		<?php	foreach ($data['furniture'] as $furniture) : 	?>
		 <div class="card mb-4 shadow-sm">
	      <div class="card-header d-flex justify-content-between">
	        <h4 class="my-0 font-weight-normal"><?= $furniture['SKU']; ?></h4>
	        <input type="checkbox" class="checkbox" name="furniturebox[]" value="<?= $furniture['id']; ?>">
	      </div>
	      <div class="card-body">
	       <h2 class="card-title text-center"><?= $furniture['name']; ?></h2>
	        <p class="mt-3 mb-3 text-center"><strong>Price:</strong> <?= $furniture['price']; ?> $</p>
	        <p class="mt-2 mb-3 text-center"><strong>Dimensions:</strong> <?= $furniture['dimensions']; ?> cm</p>
	      </div>
	    </div>
	<?php endforeach; ?>
	</div>
	<?php endif; ?>
 </form>
<?php require APPROOT . '/views/inc/footer.php'; ?>