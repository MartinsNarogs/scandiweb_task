<?php require APPROOT . '/views/inc/header.php'; ?>
		<form action="<?= URLROOT; ?>/products/add" method="post">
      <div class="d-flex  p-3 px-md-4 mb-3 justify-content-between">
        <h3 class="my-0 mr-md-auto font-weight-normal">Product Add</h3>
        <div class="my-2 my-md-0 mr-md-3">
        <button type="submit" name="submit" class="btn btn-success">Save</button>
        </div>
      </div>
          <hr>
      <p>* required field</p>
      <div class="inputs">
              <label for="SKU">SKU: *</label>
              <input type="text" name="SKU" id="SKU" placeholder="Product SKU" class="form-control <?= (!empty($data['SKU_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['SKU']; ?>">
              <span class="invalid-feedback"><?= $data['SKU_err']; ?></span>
              <label for="name">Name:</label>
              <input type="text" name="name" id="name" placeholder="Product name" class="form-control <?= (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['name']; ?>">
              <span class="invalid-feedback"><?= $data['name_err']; ?></span>
              <label for="price">Price:</label>
              <input type="text" name="price" id="price" placeholder="Product price" class="form-control <?= (!empty($data['price_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['price']; ?>">
              <span class="invalid-feedback"><?= $data['price_err']; ?></span>
              <br>
        Type of the product: 
        <select id="types" name="types" onchange="AdditAtt()">
                <!-- We use JavaScript function AdditAtt() to realize type switcher button -->
            <option value="choose">Choose a type</option>
            <option value="dvd" 
              <?= $data['types'] == 'dvd' ? 'selected' : ''; ?>
               >DVD-Disc</option>
            <option value="book" 
              <?= $data['types'] == 'book' ? 'selected' : ''; ?>
                >Book</option>
            <option value="furniture" 
              <?= $data['types'] == 'furniture' ? 'selected' : ''; ?>
                >Furniture</option>
        </select>
        <span class="invalid-feedback">
          <?= $data['types'] == 'choose' ? $data['types_err'] : ''; ?>
        </span>
        <br><br>
          <div id="dvd" class="addAtt">
                <label for="size">Size:</label>
                <input type="text" name="size" id="size" placeholder="Product size" class="form-control <?= (!empty($data['attrValue_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['attrValue']; ?>">
                <span class="invalid-feedback"><?= $data['attrValue_err']; ?></span>
              <p>Please indicate the size of the DVD-Disc in <strong>MB</strong></p>
          </div>
          <div id="book" class="addAtt">
                <label for="weight">Weight</label>
                <input type="text" name="weight" id="weight" placeholder="Product size" class="form-control <?= (!empty($data['attrValue_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['attrValue']; ?>">
                <span class="invalid-feedback"><?= $data['attrValue_err']; ?></span>
              <p>Please indicate the weight of the book in <strong>Kg</strong></p>
          </div>
          <div id="furniture" class="addAtt">
                  <label for="height">Height</label>
                <input type="text" name="height" id="height" placeholder="Product size" class="form-control <?= (!empty($data['attrValue_err'])) ? 'is-invalid' : ''; ?>">
                 <label for="width">Width</label>
                <input type="text" name="width" id="width" placeholder="Product size" class="form-control <?= (!empty($data['attrValue_err'])) ? 'is-invalid' : ''; ?>">
                  <label for="length">Length</label>
                <input type="text" name="length" id="length" placeholder="Product size" class="form-control <?= (!empty($data['attrValue_err'])) ? 'is-invalid' : ''; ?>">
                <span class="invalid-feedback"><?= $data['attrValue_err']; ?></span>
              <p>Please indicate height, width and lenght of the furniture in <strong>cm</strong></p>
          </div>
      </div>
    </form>
<?php require APPROOT . '/views/inc/footer.php'; ?>