<?php
class Book implements Types 
{
	public function getProductData($data) 
	{
		if (empty($_POST["weight"])) {
			$data['attrValue_err'] = 'Please specify the weight in KG';
		} else {
			$data['attrValue'] = check_input($_POST['weight']);
			$data['attrName'] = 'weight';
			$data['tableName'] = 'books';
		}
		return $data;
	}
}