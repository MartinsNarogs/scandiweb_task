<?php
class Dvd implements Types 
{
	public function getProductData($data) 
	{
		if (empty($_POST["size"])) {
		$data['attrValue_err'] = 'Please specify the size in MB';
		} else {
		$data['attrValue'] = check_input($_POST['size']);
		$data['attrName'] = 'size';
		$data['tableName'] = 'dvd';
		}
		return $data;
	}
}