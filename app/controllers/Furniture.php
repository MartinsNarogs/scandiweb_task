<?php
class Furniture implements Types 
{
	public function getProductData($data) 
	{
			if (empty($_POST["height"]) || empty($_POST["width"]) || empty($_POST["length"])) {
				$data['attrValue_err'] = 'Please specify the correct furniture size';
			} else {
				$height = check_input($_POST['height']);
				$width = check_input($_POST['width']);
				$length = check_input($_POST['length']);
				$data['attrValue'] = $height.'x'.$width.'x'.$length;
				$data['attrName'] = 'dimensions';
				$data['tableName'] = 'furniture';
			}
			return $data;
	}
}