<?php
class Products extends Controller
 {
 	public function __construct()
 	{
 		//Create new model
 		$this->productModel = $this->model('Product');
 	}
 	//Default method
 	public function index()
 	{
 		$books = $this->productModel->getBooks();
 		$dvd = $this->productModel->getDvd();
 		$furniture = $this->productModel->getFurniture();
 		$data = [
 			'books' => $books,
 			'dvd' => $dvd,
 			'furniture' => $furniture
 		];
 		$this->view('products/index', $data);
 	}
 	//Method to add product
 	public function add()
 	{
 		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
 			//Sanitize POST array
 			$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
 			$data = [
					'SKU' =>check_input($_POST['SKU']),
 					'name' =>check_input($_POST['name']),
 					'price' =>check_input($_POST['price']),
 					'types' =>check_input($_POST["types"]),
 					'attrValue' =>'',
 					'attrName' =>'',
 					'tableName' =>'',
 					'SKU_err' => '',
 					'name_err' => '',
 					'price_err' => '',
 					'types_err' => '',
 					'attrValue_err' => ''
				];
				//Requiring specific product class
				if (file_exists('../app/controllers/' . ucwords($data['types']).'.php')) {
					require_once '../app/controllers/'.ucwords($data['types']). '.php';
					//Validate the selected type of product and correspoding fields
					$typesSelect = new $data['types'];
					$data = $typesSelect->getProductData($data);
				} else {
					$data['types_err'] = 'Please select a product type';
				}
				//Validate SKU
				if (empty($data['SKU'])) {
					$data['SKU_err'] = 'Please enter the SKU';
				}
				if ($this->productModel->SKUcheck($data['SKU'])) {
					$data['SKU_err'] = 'The product with this SKU already exists';
				}
				//Validate Name
				if (empty($data['name'])) {
					$data['name_err'] = 'Please enter the product name';
				}
				if (empty($data['price'])) {
					$data['price_err'] = 'Please enter the product price';
				}
				//Make sure no errors
				if (empty($data['SKU_err']) && empty($data['name_err']) && empty($data['price_err']) && empty($data['types_err']) && empty($data['attrValue_err'])) {
					//Validated
					if ($this->productModel->addProduct($data)) {
						redirect('products');
					} else {
						die('Something went wrong');
					}
				} else {
					//Load the view with errors
					$this->view('products/add', $data);
				}
 		} else {
 			$data = [
 			'SKU' =>'',
 			'name' =>'',
 			'price' =>'',
 			'types' =>'',
 			'attrValue' =>'',
 			'attrName' =>'',
 			'tableName' =>''
 		];
 		$this->view('products/add', $data);
 		}
 	}
 	//Method for deleting a product
 	public function delete()
	{
 		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			//Check for arrays with checkboxes that need to be deleted
 			if (isset($_POST['dvdbox'])) {
 				$this->productModel->deleteProduct($_POST['dvdbox'], "dvd");
				}
			if (isset($_POST['bookbox'])) {
				$this->productModel->deleteProduct($_POST['bookbox'], "books");			
				}
			if (isset($_POST['furniturebox'])) {
				$this->productModel->deleteProduct($_POST['furniturebox'], "furniture");
				}
			redirect('products');
 		} else {
 			redirect('products');
 		}
 	}
 }