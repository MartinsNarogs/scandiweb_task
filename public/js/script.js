function AdditAtt() {
      //First we get the value of selected option
      var value = document.getElementById("types").value;
      //Then we get every element which we can select from
      var dvd = document.getElementById("dvd");
      var book = document.getElementById("book");
      var furniture = document.getElementById("furniture");
      //Then with the switch statement we change the display attribute to block for needed element, and to none for others
      switch(value) {
        case "choose":
        dvd.style.display = 'none';
        book.style.display = 'none';
        furniture.style.display = 'none';
        break;
        case "dvd":
        dvd.style.display = 'block';
        book.style.display = 'none';
        furniture.style.display = 'none';
        break;
        case "book":
        dvd.style.display = 'none';
        book.style.display = 'block';
        furniture.style.display = 'none';
        break;
        case "furniture":
        dvd.style.display = 'none';
        book.style.display = 'none';
        furniture.style.display = 'block';
        break;
      }
    }